//
//  Period.swift
//  WalletOptimr
//
//  Created by Quoc Dung Chu on 15/04/2017.
//  Copyright © 2017 greeny. All rights reserved.
//

import Foundation

enum Period {
  case allTime
  case year(yearNumber: Int)
  case month(monthNumber: Int, yearNumber: Int)
  case week(weekNumber: Int, monthNumber: Int, yearNumber: Int)
  case custom(from: Date, to: Date)
}
