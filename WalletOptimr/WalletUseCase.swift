//
//  WalletUseCase.swift
//  WalletOptimr
//
//  Created by Quoc Dung Chu on 15/04/2017.
//  Copyright © 2017 greeny. All rights reserved.
//

import Foundation

class WalletUseCase {
  let walletRemoteRepository: WalletRemoteRepository
  let walletLocalRepository: WalletLocalRepository
  
  init(
    walletRemoteRepository: WalletRemoteRepository,
    walletLocalRepository: WalletLocalRepository) {
    
    self.walletRemoteRepository = walletRemoteRepository
    self.walletLocalRepository = walletLocalRepository
  }
}

extension WalletUseCase: WalletInteractor {
  
  func transactions(of wallet: Wallet, in period: Period) -> [Transaction] {
    return walletLocalRepository.transactions(of: wallet)
  }
  
  func value(of wallet: Wallet, type: TransactionType, in period: Period) -> Double {
    return 0
  }
  
  func balance(of wallet: Wallet, in period: Period) -> Double {
    return transactions(of: wallet, in: period)
      .reduce(0) { $0 + $1.value }
  }
}
