//
//  Category.swift
//  WalletOptimr
//
//  Created by Quoc Dung Chu on 15/04/2017.
//  Copyright © 2017 greeny. All rights reserved.
//

class Category {
  let id: String
  let name: String
  
  init(id: String, name: String) {
    self.id = id
    self.name = name
  }
}
