//
//  Transaction.swift
//  WalletOptimr
//
//  Created by Quoc Dung Chu on 15/04/2017.
//  Copyright © 2017 greeny. All rights reserved.
//

import Foundation

enum TransactionType {
  case income, expense
}

class Transaction {
  
  let id: String
  var type: TransactionType
  var value: Double
  var note: String?
  var date: Date
  var categoryId: String
  let walletId: String

  init(
    id: String,
    type: TransactionType,
    value: Double,
    note: String?,
    date: Date,
    categoryId: String,
    walletId: String) {
    
    self.id = id
    self.type = type
    self.value = value
    self.note = note
    self.date = date
    self.categoryId = categoryId
    self.walletId = walletId
  }
}
