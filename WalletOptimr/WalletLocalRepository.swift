//
//  WalletLocalRepository.swift
//  WalletOptimr
//
//  Created by Quoc Dung Chu on 15/04/2017.
//  Copyright © 2017 greeny. All rights reserved.
//

protocol WalletLocalRepository {
  func transactions(of wallet: Wallet) -> [Transaction]
}
