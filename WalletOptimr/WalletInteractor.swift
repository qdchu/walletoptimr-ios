//
//  WalletInteractor.swift
//  WalletOptimr
//
//  Created by Quoc Dung Chu on 15/04/2017.
//  Copyright © 2017 greeny. All rights reserved.
//

import Foundation

protocol WalletInteractor {
  func transactions(of wallet: Wallet, in period: Period) -> [Transaction]
  func value(of wallet: Wallet, type: TransactionType, in period: Period) -> Double
  func balance(of wallet: Wallet, in period: Period) -> Double
}
